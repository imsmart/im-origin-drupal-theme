/**
 * 
 */
(function (domready, Drupal, drupalSettings) {

  'use strict';
  
  var toTopElement = document.createElement('div');
  
  toTopElement.innerHTML = "Наверх";
  toTopElement.setAttribute('id', 'totop');
  toTopElement.classList.add('totop');
  
  toTopElement.addEventListener('click', e => {
    window.scrollTo({
      top: 0, 
      left: 0, 
      behavior: 'smooth'
    });
  }, false);
  
  domready(function () {
    document.body.append(toTopElement);
  });
  
  window.addEventListener('scrolling.down', e => {
    toTopElement.classList.toggle('visible', false);
  }, false);
  
  window.addEventListener('scrolling.stopAtTop', e => {
    toTopElement.classList.toggle('visible', false);
  }, false);
  
  window.addEventListener('scrolling.up', e => {
    toTopElement.classList.toggle('visible', true);
  }, false);
  

})(domready, Drupal, window.drupalSettings);