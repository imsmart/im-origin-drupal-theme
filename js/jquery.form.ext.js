/**
 * 
 */
(function ($, window, Drupal, drupalSettings) {
  
  window['dataLayer']= window['dataLayer'] ||[];
  
  preventDefault = (e) => {
    if (typeof e.target['preventDefault'] != 'undefined' && e.target['preventDefault']) {
      e.preventDefault();
    }
  }
  
  $(document).on('form-submit-notify', function (e, $form, options) {
    var form = $form.get(0);
    
    var event = new Event('submit', {cancelable: true});
    
    if (typeof form.dataset['submitHandlerAttached'] == 'undefined') {
      form.addEventListener('submit', preventDefault);
      form.dataset['submitHandlerAttached'] = true;
    }
    
    form['preventDefault'] = true;
    form.dispatchEvent(event);
    form['preventDefault'] = false;
    
    
    if (form.querySelectorAll(':invalid').length > 0) {
      return;
    }
    
    dataLayer.push({
      event: 'gtm.formSubmit',
      'gtm.element': form,
    });
    
  });
})(jQuery, window, Drupal, drupalSettings);