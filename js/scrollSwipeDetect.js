/**
 * 
 */
(function(domready) {
  
  let lastScrollTop = 0;
  
  function swipedetect(el, touchendCallback, touchmoveCallback){
    
    var touchsurface = el,
    swipedir,
    startX,
    startY,
    distX,
    distY,
    threshold = 1, //required min distance traveled to be considered swipe
    restraint = 1, // maximum distance allowed at the same time in perpendicular direction
    allowedTime = 300, // maximum time allowed to travel that distance
    elapsedTime,
    startTime;
    
    touchsurface.addEventListener('touchstart', function(e){
        var touchobj = e.changedTouches[0]
        swipedir = 'none'
        dist = 0
        startX = touchobj.screenX
        startY = touchobj.screenY;
        //startTime = new Date().getTime() // record time when finger first makes contact with surface
        //e.preventDefault()
    }, false)
  
    touchsurface.addEventListener('touchmove', function(e){
      
      var touchobj = e.changedTouches[0]
      distX = touchobj.screenX - startX // get horizontal dist traveled by finger while in contact with surface
      distY = touchobj.screenY - startY // get vertical dist traveled by finger while in contact with surface
      elapsedTime = new Date().getTime() - startTime // get time elapsed
      
      if (Math.abs(distX) >= Math.abs(distY)){ 
        //condition for horizontal swipe met
        swipedir = (distX < 0)? 'left' : 'right' // if dist traveled is negative, it indicates left swipe
      }
      else { 
        // condition for vertical swipe met
        swipedir = (distY < 0)? 'up' : 'down' // if dist traveled is negative, it indicates up swipe
      }
      
      if (swipedir) {
        $(window).trigger('touchmove.' + swipedir);
      }
      
      if (touchmoveCallback) {
        touchmoveCallback(swipedir);
      }
        //e.preventDefault() // prevent scrolling when inside DIV
    }, false)
  
    touchsurface.addEventListener('touchend', function(e){
        var touchobj = e.changedTouches[0]
        distX = touchobj.screenX - startX // get horizontal dist traveled by finger while in contact with surface
        distY = touchobj.screenY - startY // get vertical dist traveled by finger while in contact with surface
        elapsedTime = new Date().getTime() - startTime // get time elapsed
        
        if (Math.abs(distX) >= Math.abs(distY)){ 
          //condition for horizontal swipe met
          swipedir = (distX < 0)? 'left' : 'right' // if dist traveled is negative, it indicates left swipe
        }
        else { 
          // condition for vertical swipe met
          swipedir = (distY < 0)? 'up' : 'down' // if dist traveled is negative, it indicates up swipe
        }
        
        if (swipedir) {
          $(window).trigger('touchend.' + swipedir);
        }
        
        if (touchendCallback) {
          touchendCallback(swipedir);
        }
        
    }, false)
  }
  
  swipedetect(window);
  
  window.swipedetect = swipedetect;
  
  window.addEventListener('scroll', e => {
    
    let target = e.currentTarget,
    scrollTop = window.pageYOffset || target.scrollTop;

    if (scrollTop > lastScrollTop) {
      var event = new Event('scrolling.down');
      window.dispatchEvent(event);
    } else {
      var event = new Event('scrolling.up');
      window.dispatchEvent(event);
    }
    
    if (!scrollTop || scrollTop == 0) {
      var event = new Event('scrolling.stopAtTop');
      window.dispatchEvent(event);
    }
    
    lastScrollTop = scrollTop;
    
  })
  
  domready(function () {

    let bodyNode = document.querySelector('body');
    
    window.addEventListener('scrolling.down', e => {

      if (bodyNode) {
        bodyNode.classList.add('scrolling-down');
        bodyNode.classList.toggle('scrolling-up', false);
      }
      
    }, false);
    
    window.addEventListener('scrolling.up', e => {

      if (bodyNode) {
        bodyNode.classList.toggle('scrolling-up', true);
        bodyNode.classList.toggle('scrolling-down', false);
      }
      
    }, false);
    
    window.addEventListener('scrolling.stopAtTop', e => {

      if (bodyNode) {
        bodyNode.classList.toggle('scrolling-down', false);
        bodyNode.classList.toggle('scrolling-up', false);
      }
      
    }, false);
    
  });
  
})(domready);