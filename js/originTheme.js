/**
 * 
 */
(function (domready, Drupal, drupalSettings) {

  'use strict';

  drupalSettings.theme = drupalSettings.theme || {};
  drupalSettings.theme.addVScrollToAnchorOffset = drupalSettings.theme.addVScrollToAnchorOffset || 0;
  
  window.addEventListener("load", function() {
    window.scrollTo(0, 0);
    var hash = window.location.hash;
    if (hash) {
      var targetElement = document.querySelector(hash);
      
      if (targetElement) {
        window.scroll({
          top: targetElement.getBoundingClientRect().top - drupalSettings.theme.addVScrollToAnchorOffset, 
          left: 0, 
          behavior: 'smooth'
        });
      }
    }
  });
  
  HTMLElement.prototype.wrap = function(wrapper) {
    this.parentNode.insertBefore(wrapper, this);
    wrapper.appendChild(this);
  }
  
  //PolyfillSection
  if (Element.prototype && !Element.prototype.toggleAttribute) {
    Element.prototype.toggleAttribute = function(name, force) {
      if(force !== void 0) force = !!force 
      
      if (this.getAttribute(name) !== null) {
        if (force) return true;
        
        this.removeAttribute(name);
        return false;
      } else {
        if (force === false) return false;
        
        this.setAttribute(name, "");
        return true;
      }
    };
  }
  
  if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = function (callback, thisArg) {
        thisArg = thisArg || window;
        for (var i = 0; i < this.length; i++) {
            callback.call(thisArg, this[i], i, this);
        }
    };
  }
  //
  
  domready(function () {
    document.querySelectorAll('table.responsive-enabled').forEach(function(table) {
      if (table.classList.contains('responsive-wrapped')) {
        return;
      }
      
      var tableWrapper = document.createElement('div');
      tableWrapper.classList.add('responsive-table-wrapper');
      table.classList.add('responsive-wrapped');
      table.wrap(tableWrapper);
    });
  });
  
//  var proxied = window.XMLHttpRequest.prototype.send;
//  window.XMLHttpRequest.prototype.send = function() {
//      console.log( arguments );
//      //Here is where you can add any code to process the request. 
//      //If you want to pass the Ajax request object, pass the 'pointer' below
//      var pointer = this
//      var intervalId = window.setInterval(function(){
//              if(pointer.readyState != 4){
//                      return;
//              }
//              console.log( pointer.responseText );
//              //Here is where you can add any code to process the response.
//              //If you want to pass the Ajax request object, pass the 'pointer' below
//              clearInterval(intervalId);
//
//      }, 1);//I found a delay of 1 to be sufficient, modify it as you need.
//      return proxied.apply(this, [].slice.call(arguments));
//  };
  
  // #SV
  
//  var XMLHttpRequestOpen = window.XMLHttpRequest.prototype.open,  
//      XMLHttpRequestSend = window.XMLHttpRequest.prototype.send;
//
//  function openReplacement(method, url, async, user, password) {  
//    this._url = url;
//    return XMLHttpRequestOpen.apply(this, arguments);
//  }
//  
//  function sendReplacement(data) {  
//    if(this.onreadystatechange) {
//      this._onreadystatechange = this.onreadystatechange;
//    }
//    /**
//     * PLACE HERE YOUR CODE WHEN REQUEST IS SENT  
//     */
//    this.onreadystatechange = onReadyStateChangeReplacement;
//    return XMLHttpRequestSend.apply(this, arguments);
//  }
//  
//  function onReadyStateChangeReplacement() {  
//    /**
//     * PLACE HERE YOUR CODE FOR READYSTATECHANGE
//     */
//    if(this._onreadystatechange) {
//      return this._onreadystatechange.apply(this, arguments);
//    }
//  }
  
//  window.XMLHttpRequest.prototype.open = openReplacement;  
//  window.XMLHttpRequest.prototype.send = sendReplacement;
  
  /**
   * Initializes the responsive behaviors for details elements.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the responsive behavior to status report specific details elements.
   */
  Drupal.behaviors.originTheme = {
    attach: function (context) {
      
      context.querySelectorAll('a[href^="#"]').forEach(function (anchorElement) {
        
        anchorElement.addEventListener('click', function (e) {
          e.preventDefault();
          
          var anch = event.target.closest('a[href^="#"]');
          if (!anch || anch.tagName.toLowerCase() !== 'a' || !anch.hash) return;
          
          var hash = anch.hash,
              toElement = document.querySelector(hash);
          
          toElement = hash === '#top' ? document.documentElement : toElement;
          
          if (!toElement) return;
          
          var addVScrollToAnchorOffset = drupalSettings.theme.addVScrollToAnchorOffset;
          var y = toElement.getBoundingClientRect().top + window.pageYOffset - addVScrollToAnchorOffset * 1;
          
          window.scrollTo({
            top: y, 
            left: 0, 
            behavior: 'smooth'
          });
          
        }, false);
        
      });
      
      $('ol[start]', context).each(function (index, olTag) {
        var element = $(olTag),
            counterStartAttrVal = element.attr('start'),
            counterStart = counterStartAttrVal * 1,
            counterStart = !isNaN(counterStart) ? counterStart - 1 : 0;
        
        if (counterStart > 0) {
          $(olTag).css('counter-reset', 'list1 ' + counterStart);
        }
      });
      
    }
  }
  
})(domready, Drupal, window.drupalSettings);