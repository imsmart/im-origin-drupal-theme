/**
 * 
 */
(function ($) {
	
	'use strict';
	
	window.responsiveData = {
	  window: {
	  	width: null,
	  	height: null,
	  }
	};
	
	function processResponsiveConf(config, $element) {
		
		const { conditions = [] } = config;
		let chacnged_dims = {};
		
		if (window.responsiveData.window.width) {
			if (window.innerWidth != window.responsiveData.window.width) {
				chacnged_dims.width = {from: window.responsiveData.window.width, to: window.innerWidth};
			}
		}
		else {
			chacnged_dims.width = {from: 0, to: window.innerWidth};
		}
		
		if (window.responsiveData.window.height) {
			if (window.innerHeight != window.responsiveData.window.height) {
				chacnged_dims.height = {from: window.responsiveData.window.height, to: window.innerHeight};
			}
		}
		else {
			chacnged_dims.height = {from: 0, to: window.innerHeight};
		}
		
		window.responsiveData.window = {
			width: window.innerWidth,
			height: window.innerHeight,
		};
		
//		if ('width' in chacnged_dims) {
//			alert(chacnged_dims.width.from + '->' + chacnged_dims.width.to);
//		}
		
		$.each(conditions, function(index, condition) {
			const { query = null, attributes = [] } = condition;
			
			
				
				$.each(chacnged_dims, function (dimName, changedDimData) {
					var regexp = new RegExp(dimName, 'gi');
					if (query.match(regexp)) {
						if (query && window.matchMedia(query).matches) {
							$.each(attributes, function(attribute, value) {
								$element.attr(attribute, value);
							});
						}
					}
				});
				
			
		});
		
	};
	
	function processResponsiveElements(e) {
		$('[data-responsive]').each(function(index, element) {
			var $element = $(element),
			    responciveConf = $element.data('responsive');
			
			if (responciveConf) {
				processResponsiveConf(responciveConf, $element);
			}
		});
	}
	
	$(document).ready(function() {
		processResponsiveElements()
	});
	
	window.addEventListener("resize", processResponsiveElements);
	
})(jQuery);

