/**
 * 
 */
(function ($) {

  'use strict';

   $(document).ready(function () {
  	 $('.form-reset-button').once('formResetButton').each(function (index, element) {
  		 var $element = $(this),
  		     parentForm = $element.parents('form').get(0);
  		 
  		 $element.click(function() {
  			 parentForm.reset();
  		 });
  		 
  	 });
   });
  
})(jQuery);