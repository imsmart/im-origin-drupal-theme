/**
 * 
 */
(function ($, window, Drupal, drupalSettings) {
  
	Drupal.Ajax.prototype.setProgressIndicatorThrobberOriginal = Drupal.Ajax.prototype.setProgressIndicatorThrobber;
	
	/**
   * Sets the throbber progress indicator.
   */
  Drupal.Ajax.prototype.setProgressIndicatorThrobber = function () {
    this.progress.element = $('<div class="linear-progress-container"><div class="linear-progress-material"><div class="bar bar1"></div><div class="bar bar2"></div></div></div>');
    if (this.progress.message) {
    	$('<div class="message">' + this.progress.message + '</div>').appendTo(this.progress.element);
    }
    this.progress.element.prependTo($('body'));
  };
  

})(jQuery, window, Drupal, drupalSettings);