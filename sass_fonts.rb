
module Sass
  module Fonts
    VERSION = "1.0.0"
    
    FONT_TYPES = {
        :woff => 'woff',
        :woff2 => 'woff2',
        :otf => 'opentype',
        :opentype => 'opentype',
        :ttf => 'truetype',
        :truetype => 'truetype',
        :svg => 'svg',
        :eot => 'embedded-opentype'
      }
      
      def font_files_by_font_name(fontName)
        fontName = fontName.value
        fontDirPath = File.join(Compass.configuration.fonts_path, fontName)
        
        return false unless Dir.exist?(fontDirPath)
        
        font_files = {}
        
        font_file_names_by_weight_and_style = {
          :thin => {
            :normal => [fontName + '-Thin', fontName + '-100'],
            :italic => [fontName + '-ThinIt', fontName + '-ThinItalic', fontName + '-100Italic']
          },
          :extralight => {
            :normal => [fontName + '-ExtraLight', fontName + '-200'],
            :italic => [fontName + '-ExtraLightIt', fontName + '-ExtraLightItalic', fontName + '-200Italic']
          },
          :light => {
            :normal => [fontName + '-Light', fontName + '-300'],
            :italic => [fontName + '-LightIt', fontName + '-LightItalic', fontName + '-300Italic']
          },
          :normal => {
            :normal => [fontName, fontName + '-Regular', fontName + '-400'],
            :italic => [fontName + '-It', fontName + '-Italic', fontName + '-400Italic']
          },
          :medium => {
            :normal => [fontName + '-Medium', fontName + '-500'],
            :italic => [fontName + '-MediumIt', fontName + '-MediumItalic', fontName + '-500Italic']
          },
          :semibold => {
            :normal => [fontName + '-SemiBold'],
            :italic => [fontName + '-SemiBoldIt', fontName + '-SemiBoldItalic']
          },
          :bold => {
            :normal => [fontName + '-Bold', fontName + '-700'],
            :italic => [fontName + '-BoldIt', fontName + '-BoldItalic', fontName + '-700Italic']
          },
          :black => {
            :normal => [fontName + '-Black', fontName + '-900'],
            :italic => [fontName + '-BlackIt', fontName + '-900kIt', fontName + '-BlackItalic', fontName + '-900Italic']
          }
        }
        
        regular_font_files = []
          
        font_file_names_by_weight_and_style.each do |weight, styles|
          styles.each do |style, file_names|
            file_names.each do |font_file_name|
              Dir.entries(fontDirPath).grep(/#{font_file_name}\./).each_with_index do |path, index|
                type = path.to_s.split('.').last.gsub(/(\?(.*))?(#(.*))?\"?/, '').to_sym
                
                if FONT_TYPES.key? type
                  font_file_path = File.join(fontName, path)
                  font_file_path = unquoted_string(font_file_path)
                  
                  weight_uq = weight#unquoted_string(weight)
                  style_uq = style#unquoted_string(style)
                  
                  font_files[weight_uq] = {} if !font_files[weight_uq]
                  font_files[weight_uq][style_uq] = {} if !font_files[weight_uq][style_uq]
                  
                  font_list = type.to_s == 'eot' ? font_file_path : list(font_url(font_file_path), identifier("format('#{FONT_TYPES[type]}')"), :space)
                  font_files[weight_uq][style_uq][unquoted_string(type.to_s)] = font_list
                else
                end
              end
            end
          end
        end
        
        font_files_map = map({})

        font_files.each do |weight, styles|
          weight_map = map({})
          styles.each do |style, types|
            types_map = map(types)
            style_map = map({unquoted_string(style.to_s) => types_map});
            weight_map = map_merge(weight_map, style_map);
          end
          font_files_map = map_merge(font_files_map, map({unquoted_string(weight.to_s) => weight_map}));
        end

        font_files_map
      end
      
      def randommm(max = Sass::Script::Number.new(100))
        Sass::Script::Number.new(rand(max.value), max.numerator_units, max.denominator_units)
      end
    
  end
end

module Sass::Script::Functions
 include Sass::Fonts
end